# Zone Parser
#### A CLI tool for reading zone files for routers

## Prerequisites
```
python 
```


## Currently available commands
- --nocolor or -nc: Removes the colored program output
- --interval or -i: Sets the interval to print out the current stats
- --find or -f: Only filters for a certain record type
- --comment or -c: Also displays the amount of comments

## Syntax
```
python parser.py <file> <other args>
```
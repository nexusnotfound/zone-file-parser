#!/usr/bin/python
import os
import argparse

COLORS = {"r": "\033[31m", "y": "\033[93m", "g": "\033[92m", "b": "\033[94m", "": "\033[0m"}
NO_COLORS = {"r": "", "y": "", "g": "", "": ""}


def clearCMD():
    os.system('cls' if os.name == 'nt' else 'clear')


def parse_args(args=[]):
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="The zone file to read")
    parser.add_argument("--nocolor", "-n","-nc", help="Adds color", action="store_true")
    parser.add_argument("--interval", "-i","-in", help="The interval to display the current stats", default=10000, type=int)
    parser.add_argument("--find", "-f","-fd", help="Only searches for a specific record type like 'NS'", default=None)
    parser.add_argument("--comment", "-c", "-sc", help="Displays number of comments", action="store_true")
    return parser.parse_args(args) if args != [] else parser.parse_args()


def getColors(args) -> dict:
    return COLORS if not args.nocolor else NO_COLORS


def display_stats(numRecords, numUDomains, numRecordsPerTypes, numComments, args):
    c = getColors(args)
    if args.find is None:
        print(f"{c["y"]}Total Records: {c["g"]}{numRecords}{c['']}")
        print(f"{c["y"]}Total unique Domains: {c["g"]}{numUDomains}{c['']}")
        print(f"{c["y"]}Total Records Per Type: {c['']}")
        if (args.comment):
            print(f"{c["y"]}Total comments: {numComments}")
        for k, v in sorted(numRecordsPerTypes.items()):
            print(f"- {c["g"]}{k}{c['']}: {c["g"]}{v}{c['']}")
    else:
        print(f"{c["y"]}Total Records of type '{args.find}': {c["g"]}{numRecordsPerTypes.get(args.find) or 0}{c['']}")


def main(args):
    numComments = 0;
    numRecords, numUDomains = 0, 0
    numRecordsPerTypes = {}
    currentDomain = ""
    ignored_record_types = ["RRSIG", "NSEC3"]

    colors = getColors(args)

    if (not os.path.isfile(args.file)):
        print(f"{colors['r']}File '{args.file}' not found!{colors['']}")
        exit()

    try:
        for i, e in enumerate(open(args.file)):
            if e.startswith(";"):
                numComments += 1
                continue
            numRecords += 1
            line = e.removesuffix("\n").replace("	", " ").split(" ")
            line = [i for i in line if i != ""]

            domain = line[0].removesuffix(".")
            record_type = line[3]

            domain_list = domain.split(".")

            if record_type not in numRecordsPerTypes:
                numRecordsPerTypes[record_type] = 0
            numRecordsPerTypes[record_type] += 1

            if record_type not in ignored_record_types:
                if len(domain_list) > 1:
                    tld = domain_list[-1]
                    domain = domain_list[-2] + tld
                    if currentDomain != domain:
                        currentDomain = domain
                        numUDomains += 1
                else:
                    tld = domain_list[0]
                    if currentDomain != tld:
                        currentDomain = tld
                        numUDomains += 1
            if i % args.interval == 0:
                clearCMD()
                print(f"{colors['y']}Parsing {colors['g']}{args.file}{colors['']}")
                display_stats(numRecords, numUDomains, numRecordsPerTypes, numComments, args)

        clearCMD()
        display_stats(numRecords, numUDomains, numRecordsPerTypes, numComments, args)
        checksum = 0
        for k, v in numRecordsPerTypes.items():
            checksum += v
        if checksum != numRecords:
            print(f"{colors["r"]}Record Checksum does not match number of records{colors['']}")
    except KeyboardInterrupt:
        print("Stopping...")

    return numRecords, numUDomains, numRecordsPerTypes


def test_main():
    print(parse_args())
    numRecords, numUDomains, numRecordsPerTypes = main(parse_args(["db.de-testzone"]))

    assert numRecords == 774
    assert numUDomains == 172


if __name__ == '__main__':
    main(parse_args())
